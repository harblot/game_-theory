<table>
    <thead><center><font color=black size=12 face="STCAIYUN">Reading Report</font></center></thead>
    <br>
    <tbody>
    <tr>
        <td><center><font color=black size=4 face="Times New Roman">title</center></td>
        <td colspan="3"><center><font color=black size=4 face="Times New Roman">A Q-Learning for Group-Based Plan of Container Transfer Scheduling <br>（基于Q-Learning的集装箱转运调度方案）</center></td>
    </tr>
    <tr>
        <td><center><font color=black size=4 face="Times New Roman">Journal</center></td>
        <td><center><font color=black size=4 face="Times New Roman">JSME INTERNATIONAL JOURNAL SERIES C-MECHANICAL SYSTEMS MACHINE ELEMENTS AND MANUFACTURING</center></td>
        <td><center><font color=black size=4 face="Times New Roman">Author</center></td>
        <td><center><font color=black size=4 face="Times New Roman">Hirashima,Yoichi</center></td>
    </tr>
    <tr>
        <td><center><font color=black size=4 face="Times New Roman">Year</center></td>
        <td><center><font color=black size=4 face="Times New Roman">JUN 2006</center></td>
        <td><center><font color=black size=4 face="Times New Roman">DOI</center></td>
        <td><center><font color=black size=4 face="Times New Roman">10.1299/jsmec.49.473</center></td>
    </tr>
    <tr>
        <td><center><div style="width: 80pt"><font color=black size=4 face="Times New Roman">Key Words</center></td>
        <td colspan="3"><font color=black size=4 face="Times New Roman">Scheduling, Learning, Dynamic Programming, Container Transfer Problem, Q-Learning, Block Stacking(集装堆码), Multi-Objective Optimization, Reinforcement Learning</td>
    </tr>
    <tr>
        <td><center><font color=black size=4 face="Times New Roman">Abstract</center></td>
        <td colspan="3"><font color=black size=4 face="kaiti"><b>背景：</b>在集装箱转运堆垛码头，集装箱到达的顺序是随机的。每个集装箱有对应的目的地，在装运上船后不能重新调度，因此在装运过程中需要按照一定的顺序对集装箱进行堆码作业。<br><b>问题：</b>在对集装箱进行堆码的过程中，集装箱堆码排序的形式随着需要堆码的集装箱数量的增加而呈指数增长，因此传统方法很难确定理想高效的集装箱堆码形式。<br><b>目标:</b>为了减少船舶等待时间，降低集装箱搬运次数，优化集装箱堆码布局以及转运作业顺序。<br><b>方法:</b>提出了一种基于集装箱堆码方式的Q-Learning算法，用于码头集装箱的转运调度。为了验证算法的有效性，对实例进行了仿真。</td>
    </tr>
</tbody></table>


***

